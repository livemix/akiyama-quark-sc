// 3.0.0-alpha

var QSC = {};

QSC.init = function(id) {
    QSC.development = false;

    QSC.isShiftedLeft = false;
    QSC.isShiftedRight = false;
    QSC.isShiftedBoth = false;

    QSC.isLibraryOpened = false;

    QSC.isSyncPushed = false;

    QSC.reverseStop = false;
    QSC.timerPlay = [false, false, false, false];
    QSC.vu_meter_timer = [false, false, false, false];
    QSC.vu_meter_previous = [false, false, false, false];

    QSC.cues = [0x17, 0x1A, 0x1D, 0x20, 0x23, 0x26, 0x57, 0x5A, 0x5D, 0x60, 0x63, 0x66, 0x17, 0x1A, 0x1D, 0x20, 0x23, 0x26];
    QSC.cuesQuickOnLoops = [0x57, 0x5A, 0x5D, 0x60, 0x63, 0x66];
    QSC.cuesQuickOnLoopsHover = [0, 0, 0, 0];
    QSC.cueHover = [0,0,0,0];

    QSC.beatLoop = [0x17, 0x1A, 0x1D, 0x20, 0x23, 0x26];
    QSC.beatLoopHover = [0,0,0,0];
    QSC.beatLoopSelected = [0,0,0,0];

    QSC.beatJumpLed = [0x58, 0x5B, 0x5E, 0x61, 0x64, 0x67, null];
    QSC.beatJumpSelected = [0,0,0,0];
    QSC.beatJump = [0,0,0,0];


    // TODO: HACK hide effects after start, devils footstaps in the dark
    engine.beginTimer(3000, function() {
        QSC.set("[EffectsModule1]", "show", 0);
        QSC.set("[EffectsModule2]", "show", 0);
    }, 1);
};

QSC.get = function(group, control) {
    return engine.getValue(group, control);
};

QSC.getChannel = function(group, control) {
    return QSC.get("[Channel"+(group+1)+"]", control);
};

QSC.set = function(group, control, value) {
    engine.setValue(group, control, value);
};

QSC.setChannel = function(group, control, value) {
    QSC.set("[Channel" + (group + 1) + "]", control, value);
};

QSC.led = function(channel, control, status) {
    var out = function(onOff) {
        midi.sendShortMsg(0x90 + channel, control, onOff);
    };

    if(status) {
        engine.beginTimer(50, function() {
            out(0x7F);
        }, 1);
    } else {
        engine.beginTimer(30, function() {
            out(0x00);
        }, 1);
    }
};

QSC.ledVu = function(channel, status) {
    midi.sendShortMsg(0xB0 + channel, 0x3D, status);
};

QSC.ledWarn = function(channel, status) {
    QSC.led(channel,0x31,status);
};

QSC.ledShiftFeature = function(status, channel) {
    QSC.led(channel,0x2F,status);
    QSC.led(4,0x03,status);
};

/*
    TODO: add visual peak support

	var vu_left_peak = engine.getValue("[Channel1]", "PeakIndicator");
	var vu_right_peak = engine.getValue("[Channel2]", "PeakIndicator");


	// get master peak value Vu-Meter
	var vu_master_peak = engine.getValue("[Master]", "PeakIndicator");

	if(QSC.development === false){
        if ( vu_left_peak === 1) {
            midi.sendShortMsg(0x90,0x0A,0x7f);
            midi.sendShortMsg(0x90,0x0B,0x7f);
            midi.sendShortMsg(0x90,0x0C,0x7f);
        } else {
            midi.sendShortMsg(0x90,0x0A,0x00);
            midi.sendShortMsg(0x90,0x0B,0x00);
            midi.sendShortMsg(0x90,0x0C,0x00);
        }

        if ( vu_right_peak === 1) {
            midi.sendShortMsg(0x91,0x0A,0x7f);
            midi.sendShortMsg(0x91,0x0B,0x7f);
            midi.sendShortMsg(0x91,0x0C,0x7f);
        } else {
            midi.sendShortMsg(0x91,0x0A,0x00);
            midi.sendShortMsg(0x91,0x0B,0x00);
            midi.sendShortMsg(0x91,0x0C,0x00);
        }

        if ( vu_master_peak === 1) {
            midi.sendShortMsg(0x90,0x3C,0x7f);
            midi.sendShortMsg(0x91,0x3C,0x7f);
        } else {
            midi.sendShortMsg(0x90,0x3C,0x00);
            midi.sendShortMsg(0x91,0x3C,0x00);
        }
*/


QSC.vuMeter = function(channel) {
    var vu_Channel_current = QSC.getChannel(channel, "VuMeter");

    if(QSC.getChannel(channel, "play_indicator") === 1){
        vu_Channel_current = (vu_Channel_current * 127);

        // TODO: find better solution that this one but its working :) and pretty good
        var setVU = function (led) {
            QSC.vu_meter_previous[channel] = led;
            midi.sendShortMsg(0xB0 + channel, 0x3D, led - 1);
        };

        if(vu_Channel_current < 24 && QSC.vu_meter_previous[channel] !== 24) {
            setVU(24);
        } else if(vu_Channel_current < 36 && QSC.vu_meter_previous[channel] !== 36) {
            setVU(36);
        } else if(vu_Channel_current < 48 && QSC.vu_meter_previous[channel] !== 48) {
            setVU(48);
        } else if(vu_Channel_current < 60 && QSC.vu_meter_previous[channel] !== 60) {
            setVU(60);
        } else if(vu_Channel_current < 72 && QSC.vu_meter_previous[channel] !== 72) {
            setVU(72);
        } else if(vu_Channel_current < 84 && QSC.vu_meter_previous[channel] !== 84) {
            setVU(84);
        } else if(vu_Channel_current < 96 && QSC.vu_meter_previous[channel] !== 96) {
            setVU(96);
        } else if(vu_Channel_current < 108 && QSC.vu_meter_previous[channel] !== 108) {
            setVU(108);
        } else if(vu_Channel_current < 120 && QSC.vu_meter_previous[channel] !== 120) {
            setVU(120);
        } else if(vu_Channel_current >= 120 && QSC.vu_meter_previous[channel] !== 127) {
            setVU(127);
        }
    }
};

// TODO: think about AUTO DJ mode
QSC.loadACBD = function(channel, control, value) {
    if (value === 127) {
        if(channel === 0  && !QSC.isShiftedLeft) QSC.setChannel(3, "LoadSelectedTrack", 1);
        if(channel === 1  && !QSC.isShiftedLeft) QSC.setChannel(4, "LoadSelectedTrack", 1);
        if(channel === 2  && !QSC.isShiftedLeft) QSC.setChannel(1, "LoadSelectedTrack", 1);
        if(channel === 3  && !QSC.isShiftedLeft) QSC.setChannel(2, "LoadSelectedTrack", 1);

        if(QSC.isShiftedLeft) {
            if(channel === 0) {
                QSC.set("[Library]","AutoDjAddBottom",1);
            } else if(channel === 1) {
                QSC.set("[Library]","AutoDjAddTop",1);
            }

            QSC.ledShiftFeature(channel, true);
        }
    } else {
        QSC.ledShiftFeature(channel, false);
    }
};

// TODO: refactor effects add 3 and 4 control global !!!! global !!!!
QSC.effectUnit1Prev = function(channel, control, value) {
    if (value === 127) {
        QSC.set("[EffectRack1_EffectUnit1_Effect1]", "prev_effect", 1);
        QSC.led(0, 0x0E, true);
    } else {
        QSC.set("[EffectRack1_EffectUnit1_Effect1]", "prev_effect", 0);
        QSC.led(0, 0x0E, false);
    }
};

QSC.effectUnit2Prev = function(channel, control, value) {
    if (value === 127) {
        QSC.set("[EffectRack1_EffectUnit2_Effect1]", "prev_effect", 1);
        QSC.led(1, 0x0E, true);
    } else {
        QSC.set("[EffectRack1_EffectUnit2_Effect1]", "prev_effect", 0);
        QSC.led(1, 0x0E, false);
    }
};

QSC.effectUnit1Next = function(channel, control, value) {
    if (value === 127) {
        QSC.set("[EffectRack1_EffectUnit1_Effect1]", "next_effect", 1);
        QSC.led(0, 0x0F, true);
    } else {
        QSC.set("[EffectRack1_EffectUnit1_Effect1]", "next_effect", 0);
        QSC.led(0, 0x0F, false);
    }
};

QSC.effectUnit2Next = function(channel, control, value) {
    if (value === 127) {
        QSC.set("[EffectRack1_EffectUnit2_Effect1]", "next_effect", 1);
        QSC.led(1, 0x0F, true);
    } else {
        QSC.set("[EffectRack1_EffectUnit2_Effect1]", "next_effect", 0);
        QSC.led(1, 0x0F, false);
    }
};

QSC.effectUnit1Activate = function(channel, control, value) {
    if (value === 127) {
        if(QSC.get("[EffectRack1_EffectUnit1_Effect1]", "enabled") === 1) {
            QSC.set("[EffectRack1_EffectUnit1_Effect1]", "enabled", 0);
            QSC.set("[EffectsModule1]", "show", 0);
            QSC.led(0, 0x10, false);
        } else {
            QSC.set("[EffectRack1_EffectUnit1_Effect1]", "enabled", 1);
            QSC.set("[EffectsModule1]", "show", 1);
            QSC.led(0, 0x10, true);
        }
    }
};

QSC.effectUnit2Activate = function(channel, control, value) {
    if (value === 127) {
        if(QSC.get("[EffectRack1_EffectUnit2_Effect1]", "enabled") === 1) {
            QSC.set("[EffectRack1_EffectUnit2_Effect1]", "enabled", 0);
            QSC.set("[EffectsModule2]", "show", 0);
            QSC.led(1, 0x10, false);
        } else {
            QSC.set("[EffectRack1_EffectUnit2_Effect1]", "enabled", 1);
            QSC.set("[EffectsModule2]", "show", 1);
            QSC.led(1, 0x10, true);
        }
    }
};

QSC.effectUnit1ActivatePush = function(channel, control, value) {
    if (value === 127) {
        QSC.set("[EffectRack1_EffectUnit1_Effect1]", "enabled", 1);
        QSC.set("[EffectsModule1]", "show", 1);
        QSC.led(0, 0x10, true);
    } else {
        QSC.set("[EffectRack1_EffectUnit1_Effect1]", "enabled", 0);
        QSC.set("[EffectsModule1]", "show", 0);
        QSC.led(0, 0x10, false);
    }
};

QSC.effectUnit2ActivatePush = function(channel, control, value) {
    if (value === 127) {
        QSC.set("[EffectRack1_EffectUnit2_Effect1]", "enabled", 1);
        QSC.set("[EffectsModule2]", "show", 1);
        QSC.led(1, 0x10, true);
    } else {
        QSC.set("[EffectRack1_EffectUnit2_Effect1]", "enabled", 0);
        QSC.set("[EffectsModule2]", "show", 0);
        QSC.led(1, 0x10, false);
    }
};

QSC.autoLeft = function(channel, control, value) {
    if (value === 127) {
        if(QSC.isShiftedLeft || QSC.isShiftedRight) {
            if(QSC.get("[Deck3]", "show") === 1) {
                QSC.set("[Deck3]", "show", 0);
            } else {
                QSC.set("[Deck3]", "show", 1);
            }
        } else {
            if(QSC.get("[Deck1]", "show") === 1) {
                QSC.set("[Deck1]", "show", 0);
            } else {
                QSC.set("[Deck1]", "show", 1);
            }
        }
    }
};

QSC.autoRight = function(channel, control, value) {
    if (value === 127) {
        if(QSC.isShiftedLeft || QSC.isShiftedRight) {
            if(QSC.get("[Deck4]", "show") === 1) {
                QSC.set("[Deck4]", "show", 0);
            } else {
                QSC.set("[Deck4]", "show", 1);
            }
        } else {
            if(QSC.get("[Deck2]", "show") === 1) {
                QSC.set("[Deck2]", "show", 0);
            } else {
                QSC.set("[Deck2]", "show", 1);
            }
        }
    }
};

// TODO: refactor eject join with timer
QSC.eject = function (channel, control, value) {
    if (value === 127) {
        if(QSC.getChannel(channel, "play_indicator") === 1 && (QSC.isShiftedLeft || QSC.isShiftedRight)) {
            QSC.setChannel(channel, "play", 0);
        } else {
            QSC.ledWarn(channel, true);
        }

        QSC.setChannel(channel, "eject", 1);
    } else {
        QSC.setChannel(channel, "eject", 0);

        if(QSC.getChannel(channel, "play_indicator") === 0) {
            QSC.led(channel, 0x34, false);
            QSC.led(channel, 0x37, false);
            QSC.led(channel, 0x3D, false);
            QSC.led(channel, 0x2F, false);
            QSC.ledVu(channel, 0x00);

            engine.stopTimer(QSC.timerPlay[channel]);
            engine.stopTimer(QSC.vu_meter_timer[channel]);

            QSC.set("[Deck" + (channel+1) + "]", "show", 0);
        }

        QSC.ledWarn(channel, false);
    }
};

// TODO: add control sentivity ... batter this one
QSC.wheelTouch = function (channel, control, value) {
    if ((value) === 127 && ((QSC.isShiftedLeft && (channel === 0 || channel === 2)) || (QSC.isShiftedRight && (channel === 1 || channel === 3) ))) {
        var alpha = 1.0/32;
        var beta = alpha/32;
        engine.scratchEnable(channel + 1, 128, 33+1/3, alpha, beta);
    } else {
        engine.scratchDisable(channel + 1);
    }
};

QSC.wheelTurn = function (channel, control, value) {
    var newValue = value - 64;

    if (QSC.isShiftedBoth) {
        QSC.shiftWheelTurn(channel, control, value);
    } else if (engine.isScratching(channel + 1) && ((QSC.isShiftedLeft && (channel === 0 || channel === 2)) || (QSC.isShiftedRight && (channel === 1 || channel === 3) ))) {
        engine.scratchTick(channel + 1, newValue);
    } else {
        QSC.setChannel(channel, "jog", newValue/6.66);
        engine.scratchDisable(channel + 1);
    }
};

QSC.shiftWheelTurn = function (channel, control, value) {
    var newValue = value - 64;
    var position = QSC.getChannel(channel, "playposition")*100;

    if(position === 100 || position >= 100) {
        QSC.getChannel(channel, "eject", 1);
        QSC.led(channel, 0x34, false);
        QSC.led(channel, 0x37, false);
        QSC.led(channel, 0x3D, false);
        QSC.ledVu(channel, 0x00);
        QSC.getChannel(channel, "eject", 0);
        engine.stopTimer(QSC.timerPlay[channel]);
        engine.stopTimer(QSC.vu_meter_timer[channel]);
        QSC.set("[Deck" + (channel+1) + "]", "show", 0);
    }

    QSC.setChannel(channel, "jog", newValue*33);
    engine.scratchDisable(channel+1);
};

QSC.isTrackLoaded = function(channel) {
    return QSC.getChannel(channel, "track_loaded");
};

// TODO: refactor play logic, CRITICAL !!!
QSC.play = function(channel, control, value) {
    var local_deck = channel + 1;
    if(value === 127) {
        if(QSC.get("[Channel"+local_deck+"]", "track_loaded") === 1 ) {
            if(QSC.get("[Channel"+local_deck+"]", "play_indicator") === 0 ) {
                if(QSC.isShiftedLeft && (local_deck === 1 || local_deck === 3)) {
                    print("&&&&& - 1");
                    engine.stopTimer(QSC.timerPlay[channel]);
                    engine.stopTimer(QSC.vu_meter_timer[channel]);
                    QSC.setChannel(channel,"play",1);
                    QSC.led(channel, 0x34, true);
                    QSC.ledShiftFeature(channel, true);
                    QSC.reverseStop = false;
                    QSC.timerPlay[channel] = engine.beginTimer(1000,"QSC.playTimer(" + channel + ")");
                    QSC.vu_meter_timer[channel] = engine.beginTimer(50,"QSC.vuMeter("+channel+")");
                } else if(QSC.isShiftedRight && (local_deck === 2 || local_deck === 4)) {
                    print("&&&&& - 2");
                    QSC.reverseStop = false;
                    engine.stopTimer(QSC.timerPlay[channel]);
                    QSC.timerPlay[channel] = engine.beginTimer(1000,"QSC.playTimer(" + channel + ")");
                    engine.stopTimer(QSC.vu_meter_timer[channel]);
                    QSC.vu_meter_timer[channel] = engine.beginTimer(50,"QSC.vuMeter("+channel+")");
                    QSC.set("[Channel"+local_deck+"]","play",1);
                    QSC.led(channel, 0x34, true);
                    QSC.ledShiftFeature(channel, true);
                }else if(QSC.isShiftedRight && (local_deck === 1 || local_deck === 3)) {
                    print("&&&&& - 3");
                    QSC.reverseStop = true;
                    QSC.set("[Channel"+local_deck+"]","play",1);
                    QSC.set("[Channel"+local_deck+"]","reverse", 1);
                    QSC.led(channel, 0x34, true);
                    QSC.ledShiftFeature(channel, true);
                }  else if(QSC.isShiftedLeft && (local_deck === 2 || local_deck === 4)) {
                    print("&&&&& - 4");
                    QSC.reverseStop = true;
                    QSC.set("[Channel"+local_deck+"]","play",1);
                    QSC.set("[Channel"+local_deck+"]","reverse", 1);
                    QSC.led(channel, 0x34, true);
                    QSC.ledShiftFeature(channel, true);
                } else {
                    print("&&&&& - 5");
                    QSC.reverseStop = false;
                    engine.stopTimer(QSC.timerPlay[channel]);
                    QSC.timerPlay[channel] = engine.beginTimer(1000,"QSC.playTimer(" + channel + ")");
                    engine.stopTimer(QSC.vu_meter_timer[channel]);
                    QSC.vu_meter_timer[channel] = engine.beginTimer(50,"QSC.vuMeter("+channel+")");
                    engine.softStart(local_deck, true, 1);
                    QSC.led(channel, 0x34, true);
                    QSC.set("[Deck"+(channel+1)+"]", "show", 1)
                }
            } else {
                if(QSC.isShiftedLeft && (local_deck === 1 || local_deck === 3)) {
                    QSC.reverseStop = false;
                    engine.stopTimer(QSC.timerPlay[channel]);
                    engine.stopTimer(QSC.vu_meter_timer[channel]);
                    QSC.set("[Channel"+local_deck+"]", "play", 0);
                    QSC.led(channel, 0x34, false);
                    QSC.ledVu(channel, 0x3D, false);
                } else if(QSC.isShiftedRight && (local_deck === 1 || local_deck === 3)) {
                    QSC.reverseStop = false;
                    QSC.set("[Channel"+local_deck+"]", "reverse", 1);
                } else if(QSC.isShiftedRight && (local_deck === 2 || local_deck === 4)) {
                    QSC.reverseStop = false;
                    engine.stopTimer(QSC.timerPlay[channel]);
                    engine.stopTimer(QSC.vu_meter_timer[channel]);
                    QSC.set("[Channel"+local_deck+"]", "play", 0);
                    QSC.led(channel, 0x34, false);
                    QSC.ledVu(channel, 0x3D, false);
                } else if(QSC.isShiftedLeft && (local_deck === 2 || local_deck === 4)) {
                    QSC.reverseStop = false;
                    QSC.set("[Channel"+local_deck+"]", "reverse", 1);
                } else {
                    QSC.reverseStop = false;
                    engine.brake(local_deck, true, 0.333);
                    QSC.led(channel, 0x34, false);
                }
            }
        } else {
            QSC.led(channel, "0x34", false);
            QSC.loadSelectedTrack(channel);
        }
    } else if(value === 0) {
        if(QSC.get("[Channel"+local_deck+"]", "track_loaded") === 1 ) {
            if(QSC.get("[Channel"+local_deck+"]", "reverse") === 1) {
                QSC.set("[Channel"+local_deck+"]", "reverse", 0);
                if(QSC.reverseStop && (QSC.isShiftedLeft || QSC.isShiftedRight)) {
                    QSC.reverseStop = false;
                    engine.stopTimer(QSC.timerPlay[channel]);
                    engine.stopTimer(QSC.vu_meter_timer[channel]);
                    QSC.ledVu(channel, 0x3D, false);
                    QSC.set("[Channel"+local_deck+"]", "play", 0);
                }
            } else if(!QSC.isShiftedLeft && (local_deck === 1 || local_deck === 3)) {
                engine.softStart(local_deck, true, 1);
                QSC.led(channel, "0x34", true);

            } else if(!QSC.isShiftedRight && (local_deck === 2 || local_deck === 4)) {
                engine.softStart(local_deck, true, 1);
                QSC.led(channel, "0x34", true);
            }
        }
    }
};

QSC.playTimerBlinkOn = function(channel, time) {
    engine.beginTimer(time,"QSC.playTimerBlinkOff(" + channel + ")", 1);
    QSC.led(channel, 0x37, true);
};

QSC.playTimerBlinkOff = function(channel) {
    QSC.led(channel, 0x37, false);
};

// TODO: better timer + join with eject global
QSC.playTimer = function(channel) {
    var position = QSC.getChannel(channel, "playposition")*100;

    if(position === 100) {
        QSC.setChannel(channel, "eject", 1);
        QSC.led(channel, 0x34, false);
        QSC.led(channel, 0x37, false);
        QSC.ledVu(channel, 0x00);
        QSC.setChannel(channel, "eject", 0);
        engine.stopTimer(QSC.timerPlay[channel]);
        engine.stopTimer(QSC.vu_meter_timer[channel]);
        QSC.set("[Deck"+(channel+1)+"]", "show", 0);
    } else if(position >= 75 && position < 90) {
        engine.beginTimer(500,"QSC.playTimerBlinkOn(" + channel + ", 500)", 1);
    } else if(position >= 85 && position < 95) {
        engine.beginTimer(333,"QSC.playTimerBlinkOn(" + channel + ", 666)", 1);
    } else if(position >= 95) {
        QSC.led(channel, 0x37, false);
        QSC.led(channel, 0x37, true);
    }
};

QSC.showLibrary = function() {
    if(!QSC.isLibraryOpened) {
        QSC.set("[library]", "show", 1);
        QSC.set("[Hifi]", "show", 0);
        QSC.isLibraryOpened = true;
    } else {
        QSC.set("[library]", "show", 0);
        QSC.set("[Hifi]", "show", 1);
        QSC.isLibraryOpened = false;
    }
};

QSC.loadSelectedTrack = function(channel) {
        if(channel >= 0 || channel < 4) {
            QSC.setChannel(channel, "LoadSelectedTrack", 1);
            QSC.set("[Deck"+(channel+1)+"]", "show", 1)
        } else {
            QSC.set("[Playlist]", "LoadSelectedIntoFirstStopped", 1);

            if(QSC.get("[Channel1]", "play_indicator") === 0 ) engine.beginTimer(1000, function() { if(QSC.get("[Channel1]", "track_loaded") === 1 ) QSC.set("[Deck1]", "show", 1) }, 1);
            if(QSC.get("[Channel2]", "play_indicator") === 0 ) engine.beginTimer(1000, function() { if(QSC.get("[Channel2]", "track_loaded") === 1 ) QSC.set("[Deck2]", "show", 1) }, 1);
            if(QSC.get("[Channel3]", "play_indicator") === 0 ) engine.beginTimer(1000, function() { if(QSC.get("[Channel3]", "track_loaded") === 1 ) QSC.set("[Deck3]", "show", 1) }, 1);
            if(QSC.get("[Channel4]", "play_indicator") === 0 ) engine.beginTimer(1000, function() { if(QSC.get("[Channel4]", "track_loaded") === 1 ) QSC.set("[Deck4]", "show", 1) }, 1);
            // TODO: find better solution than this one but not FOR hmm...
        }
};

QSC.playlistTrackSelect = function(channel, group, value) {
    if(value === 127) {
        if(QSC.isShiftedLeft) {
            QSC.set("[Library]", "ChooseItem", 1);
        } else if(QSC.isLibraryOpened) {
            QSC.loadSelectedTrack();
            QSC.showLibrary();
        } else if(QSC.isShiftedRight) {
            QSC.loadSelectedTrack();
        } else {
            QSC.showLibrary();
        }
    } else if(value === 0) {
    } else {
        if(value === 65 && QSC.isShiftedLeft) {
            QSC.set("[Playlist]", "SelectNextPlaylist", 1);
        } else if(value === 63 && QSC.isShiftedLeft) {
            QSC.set("[Playlist]", "SelectPrevPlaylist", 1);
        } else if(value === 65 && QSC.isShiftedRight) {
            QSC.setChannel(channel, "waveform_zoom_down", 1)
        } else if(value === 63 && QSC.isShiftedRight) {
            QSC.setChannel(channel, "waveform_zoom_up", 1)
        } else if(value === 65) {
            QSC.set("[Playlist]", "SelectNextTrack", 1);
        } else if(value === 63) {
            QSC.set("[Playlist]", "SelectPrevTrack", 1);
        }
    }
};


// TODO: make it easy
var loops = [1, 2, 4, 8, 16, 32];

QSC.beatLoops = function(channel, group, value) {
    if(value === 127) {
        if(QSC.isShiftedLeft && QSC.isShiftedRight) {
            QSC.setChannel(channel, "hotcue_"+(QSC.cueHover[channel]+1)+"_clear", 1);
        } else if(QSC.isShiftedRight) {
            QSC.setChannel(channel, "beatjump_"+QSC.beatJump[QSC.beatJumpSelected[channel]]+"_forward", 1);
        } else if(QSC.isShiftedLeft) {
            QSC.setChannel(channel, "hotcue_"+(QSC.cueHover[channel]+1)+"_activate", 1);
            QSC.setChannel(channel, "play", 0);

            // TODO: not led on active cue
            if(QSC.getChannel(channel, "play_indicator") === 0 ) {
                engine.stopTimer(QSC.timerPlay[channel]);
                engine.stopTimer(QSC.vu_meter_timer[channel]);
                QSC.timerPlay[channel] = engine.beginTimer(1000,"QSC.playTimer("+channel+")");
                QSC.vu_meter_timer[channel] = engine.beginTimer(50,"QSC.vuMeter("+channel+")");
                QSC.led(channel, 0x34, true);
            } else {
                QSC.setChannel(channel, "play", 0);
                engine.stopTimer(QSC.timerPlay[channel]);
                engine.stopTimer(QSC.vu_meter_timer[channel]);
                QSC.led(channel, 0x34, false);
                QSC.ledVu(channel, 0x3D, false);
            }
        } else {
            QSC.setChannel(channel, "beatloop_"+loops[QSC.beatLoopHover[channel]]+"_toggle", 1);
        }
        if(QSC.getChannel(channel,"loop_enabled") === 1) {
            QSC.led(channel, 0x2C, true);
        } else {
            QSC.led(channel, 0x2C, false);
        }
    } else {
        var save = function(channel, control) {
            if(control === "cueHover") {
                QSC.cuesQuickOnLoopsHover[channel] = QSC.cueHover[channel];
                QSC.led(channel, QSC.cuesQuickOnLoops[QSC.cueHover[channel]], true);
            } else if(control === "beatLoop") {
                QSC.beatLoopSelected[channel] = QSC.beatLoopHover[channel];
                QSC.led(channel, QSC.beatLoop[QSC.beatLoopHover[channel]], true);
            } else {
                QSC.beatJumpSelected[channel] = QSC.beatJump[channel];
                QSC.led(channel, QSC.beatJumpLed[QSC.beatJump[channel]], true);
            }
        };

        // TODO: add more than 6 cues but not shift, something different
        if(QSC.isShiftedLeft) {
            if (value === 65 && QSC.cueHover[channel] < 5) {
                QSC.led(channel, QSC.cuesQuickOnLoops[QSC.cueHover[channel]]);
                QSC.cueHover[channel]++;
                save(channel, "cueHover");
            } else if (value === 63 && QSC.cueHover[channel] > 0) {
                QSC.led(channel, QSC.cuesQuickOnLoops[QSC.cueHover[channel]]);
                QSC.cueHover[channel]--;
                save(channel, "cueHover");
            } else if (value === 63 && QSC.cueHover[channel] === 0) {
                QSC.led(channel,  QSC.cuesQuickOnLoops[0]);
                QSC.cueHover[channel] = 5;
                save(channel, "cueHover");
            } else if (value === 65 && QSC.cueHover[channel] === 5) {
                QSC.led(channel, QSC.cuesQuickOnLoops[5]);
                QSC.cueHover[channel] = 0;
                save(channel, "cueHover");
            }
        } else if(QSC.isShiftedRight) {
            if(value === 65) {
                QSC.setChannel(channel, "waveform_zoom_down", 1)
            } else if(value === 63) {
                QSC.setChannel(channel, "waveform_zoom_up", 1)
            }
        } else {
            if (value === 65 && (QSC.beatLoopHover[channel] < 5)) {
                QSC.led(channel, QSC.beatLoop[QSC.beatLoopHover[channel]], false);
                QSC.beatLoopHover[channel]++;
                save(channel, "beatLoop");
            } else if (value === 63 && QSC.beatLoopHover[channel] > 0) {
                QSC.led(channel, QSC.beatLoop[QSC.beatLoopHover[channel]], false);
                QSC.beatLoopHover[channel]--;
                save(channel, "beatLoop");
            } else if (value === 63 && QSC.beatLoopHover[channel] === 0) {
                QSC.led(channel, QSC.beatLoop[0], false);
                QSC.beatLoopHover[channel] = 5;
                save(channel, "beatLoop");
            } else if (value === 65 && QSC.beatLoopHover[channel] === 5) {
                QSC.led(channel, QSC.beatLoop[5], false);
                QSC.beatLoopHover[channel] = 0;
                save(channel, "beatLoop");
            }
        }

        /*  TODO: think about add beat jump to right shift deck? but right now  waveform_zoom

            if (value === 65 && QSC.beatJump[channel] < 6) {
                ledChange(channel, QSC.beatJumpLed[QSC.beatJump[channel]]);
                QSC.beatJump[channel]++;
                save(channel);
            } else if (value === 63 && QSC.beatJump[channel] > 0) {
                ledChange(channel, QSC.beatJumpLed[QSC.beatJump[channel]]);
                QSC.beatJump[channel]--;
                save(channel);
            } else if (value === 63 && QSC.beatJump[channel] === 0) {
                ledChange(channel,  QSC.beatJumpLed[0]);
                QSC.beatJump[channel] = 6;
                save(channel);
            } else if (value === 65 && QSC.beatJump[channel] === 6) {
                ledChange(channel, QSC.beatJumpLed[5]);
                QSC.beatJump[channel] = 0;
                save(channel);
            }
            */
    }
};

// TODO: refactor shift
QSC.shiftLeft = function(channel, control, value) {
    if (value === 127) {
        QSC.isShiftedLeft = true;
        QSC.led(channel, "0x35", true);
        QSC.led(channel, QSC.cuesQuickOnLoops[QSC.cuesQuickOnLoopsHover[channel]], true);
        QSC.led((channel+1), QSC.cuesQuickOnLoops[QSC.cuesQuickOnLoopsHover[(channel+1)]], true);
        QSC.led(channel, QSC.beatLoop[QSC.beatLoopHover[channel]], false);
        QSC.led((channel+1), QSC.beatLoop[QSC.beatLoopHover[(channel+1)]], false);
    } else {
        QSC.led(channel, QSC.cuesQuickOnLoops[QSC.cuesQuickOnLoopsHover[channel]], false);
        QSC.led((channel+1), QSC.cuesQuickOnLoops[QSC.cuesQuickOnLoopsHover[(channel+1)]], false);
        QSC.led(channel, QSC.beatLoop[QSC.beatLoopHover[channel]], true);
        QSC.led((channel+1), QSC.beatLoop[QSC.beatLoopHover[(channel+1)]], true);
        if(!QSC.isShiftedRight) {
            QSC.isShiftedLeft = false;
            QSC.led(channel, 0x35, false);
            QSC.ledShiftFeature(false, channel);
        } else if(QSC.isShiftedBoth) {
            QSC.isShiftedBoth = false;
            QSC.isShiftedLeft = false;
            QSC.ledShiftFeature(false, channel);
            QSC.led(channel, 0x35, false);
        } else {
            QSC.isShiftedBoth = true;
            QSC.ledShiftFeature(true, channel);
            QSC.led(channel, QSC.cuesQuickOnLoops[QSC.cuesQuickOnLoopsHover[channel]], true);
            QSC.led((channel+1), QSC.cuesQuickOnLoops[QSC.cuesQuickOnLoopsHover[(channel+1)]], true);
            QSC.led(channel, QSC.beatLoop[QSC.beatLoopHover[channel]], false);
            QSC.led((channel+1), QSC.beatLoop[QSC.beatLoopHover[(channel+1)]], false);
        }
    }
};

QSC.shiftRight = function(channel, control, value) {
    if(value === 127) {
        QSC.isShiftedRight = true;
        QSC.led(channel, 0x35, true);
        QSC.led(channel, QSC.beatLoop[QSC.beatLoopHover[channel]], false);
        QSC.led((channel-1), QSC.beatLoop[QSC.beatLoopHover[channel]], false);
    } else {
        QSC.led(channel, QSC.beatLoop[QSC.beatLoopHover[channel]], true);
        QSC.led((channel-1), QSC.beatLoop[QSC.beatLoopHover[(channel-1)]], true);
        if(!QSC.isShiftedLeft) {
            QSC.isShiftedRight = false;
            QSC.led(channel, 0x35, false);
            QSC.ledShiftFeature(false, channel);
        } else if(QSC.isShiftedBoth) {
            QSC.isShiftedBoth = false;
            QSC.isShiftedRight = false;
            QSC.led(channel, 0x35, false);
            QSC.ledShiftFeature(false, channel);
        }
    }
};

// TODO: sync as new control way to controls visual objects?
QSC.sync = function(channel, control, value) {
    if(value === 127) {
        QSC.isSyncPushed = true;
        if(QSC.isShiftedBoth) {
            if(QSC.getChannel(channel, "sync_enabled") === 0) {
                QSC.setChannel(channel, "sync_enabled", 1);
            } else {
                QSC.setChannel(channel, "sync_enabled", 0);
            }
        } else if(QSC.getChannel(channel, "sync_enabled") === 1) {
            QSC.setChannel(channel, "sync_enabled", 0);
        } else if(QSC.isShiftedLeft || QSC.isShiftedRight) {
            QSC.setChannel(channel, "rate_set_default", 1);
        } else {
            QSC.setChannel(channel, "beatsync_tempo", 1);
        }

        QSC.led(channel, 0x36, true);
    } else {
        QSC.isSyncPushed = false;

        if(QSC.getChannel(channel, "sync_enabled") === 0) QSC.led(channel, 0x36, false);
    }
};

// TODO: add ejects all on shutdown
QSC.shutdown = function(id) {

};